<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'twwebpro_wp375');

/** MySQL database username */
define('DB_USER', 'twwebpro_wp375');

/** MySQL database password */
define('DB_PASSWORD', 'S963E(kPQ[');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sfwjpa9t1a4tnegbazkge7xpj5n9c2cgxmxq0vjgcgnxwhzhiryebqqniys8w7rw');
define('SECURE_AUTH_KEY',  'zpz5qkscg5ulmnstoi2hydpsv4h7vw3dzexhbmlb9k9n3qbj4fin4raaqoioriit');
define('LOGGED_IN_KEY',    'xheabailgb6akaj2mmuld9fnt5zutwtgab0hhd82gv6llcp4ear2v1rjk3ceve1n');
define('NONCE_KEY',        'ew03jguzhssvlgwck0byrngngj97qohjj0demnvh4jspqqurz245cwyznqujsxwx');
define('AUTH_SALT',        'a62wfmmchxgofgkb8dgc4rtcnbfszsnfkdv80rtz9fyx2e0mhl5u789juhao5jar');
define('SECURE_AUTH_SALT', 'zsunqp3rr8kd0rqbjh2flkks418g1mjuczxqlm27pwypqq0figlfbh5mz1r5tpbp');
define('LOGGED_IN_SALT',   'fdw6j6bvhnff2cd1aezac5t6eiydr8yhpzwsgfd9moqisdowl46yfnsxfpcc9ei7');
define('NONCE_SALT',       'odf7txvf9bnwhow34kxv16wwoqbfnra3vb0xdn0vckmq6wuxuv09isso6sacuro5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp0r_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
